# Additional task (*)
Rebasing is the process of moving or combining a sequence of commits to a new base commit. 
Rebasing is most useful and easily visualized in the context of a feature branching workflow.

The general process can be visualized as the following:
![first task](./rebase.png)

The primary reason for rebasing is to maintain a linear project history.
Git Rebase itself is not seriously dangerous. 
The real danger cases arise when executing history rewriting interactive rebases and force pushing the results to a remote branch that's shared by other users. 
This is a pattern that should be avoided as it has the capability to overwrite other remote users' work when they pull.

