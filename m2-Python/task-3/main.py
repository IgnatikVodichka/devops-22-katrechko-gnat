import argparse
import string
from random import sample

parser = argparse.ArgumentParser()
group_exclusive1 = parser.add_mutually_exclusive_group(required=False)
group_exclusive1.add_argument("-l", "--length", metavar="", action="store", type=int,
                              help="""Set length of password and generate random password 
                              from set {small lateral ASCII, big lateral ASCII, digit}""")
group_exclusive1.add_argument("-t", "--template", metavar="", action="store",
                              help="Set template for generate passwords")
group_exclusive1.add_argument("-f", "--file", metavar="", action="store",
                              help="Getting list of patterns from file and generate for each random password")
parser.add_argument("-c", "--count", metavar="", action="store", type=int, default=1, help="Number of passwords")
parser.add_argument("-v", "--verbose", action="count", default=0,
                    help="Verbose mode (-v |-vv |-vvv )")
args = parser.parse_args()

'''exclusive group is not enough, so we have to check if 
arguments can be used together and length can't be less than 6 characters.'''
if args.count > 1 and args.file is not None:
    args.count = 0
    parser.error("argument -c/--count: not allowed with argument -f/--file")
if args.length is not None:
    if args.length < 6:
        parser.error("argument -l/--length: not allowed to be less than '6'")


def main():

    alphabet_lower = string.ascii_lowercase
    alphabet_upper = string.ascii_uppercase
    numbers = string.digits
    symbols = string.punctuation

    def no_args(param):
        """checking which arguments are entered and returning it."""
        for arg in vars(args):
            if getattr(args, arg) is not None and arg != "verbose" and arg != "count":
                return arg

    def generate_password_with_length(length=6):
        """generating and returning password with default length if no arguments but if at least length is given."""
        if length == 6:
            password = alphabet_upper + alphabet_lower
            return "".join(sample(password, length))
        elif length > 6:
            password = alphabet_upper + numbers + alphabet_lower
            return "".join(sample(password, length))

    def sub_template(sub_string):
        """takes substring and generates and returns random letters, digits,
            punctuations, depending on what's inside the brackets"""
        samples = ""
        new_substring = ""
        idx = 0
        for i, s in enumerate(sub_string):
            if s.isalpha():
                samples += s
            if s.isdigit() or sub_string[i] == "]" and sub_string[i+1] == "%":
                idx = i
                for k in range(int(s) if s.isdigit() else 1):
                    sub = "".join((sample(samples, 1)))
                    if sub == "A":
                        new_substring += "".join((sample(alphabet_upper, 1)))
                    elif sub == "a":
                        new_substring += "".join((sample(alphabet_lower, 1)))
                    elif sub == "d":
                        new_substring += "".join((sample(numbers, 1)))
                    elif sub == "p":
                        new_substring += "".join((sample(symbols, 1)))
                return new_substring, idx

    def generate_password_by_template(template):
        """password with template generator"""
        password = ""
        i = 0
        while i < len(template):
            if template[i] == "%":
                i += 1
                continue
            # as soon as it finds open bracket it will send the following substring to another function
            elif template[i] == "[":
                password += sub_template(template[i:])[0]
                i += sub_template(template[i:])[1]
            elif template[i] == "A":
                password += "".join((sample(alphabet_upper, int(template[i + 1] if template[i+1] != "%" else 1))))
            elif template[i] == "a":
                password += "".join((sample(alphabet_lower, int(template[i + 1] if template[i+1] != "%" else 1))))
            elif template[i] == "d":
                password += "".join((sample(numbers, int(template[i + 1] if template[i+1] != "%" else 1))))
            elif template[i] == "p":
                password += "".join((sample(symbols, int(template[i + 1] if template[i+1] != "%" else 1))))
            elif template[i] in symbols:
                password += template[i]
            i += 1
        return password
    arg = no_args(args)
    '''takes a count arg (default is 1 if not given through CLI) and creates passwords in a loop that many times'''
    for i in range(args.count):
        if arg is None:
            print(generate_password_with_length())
        elif arg == "length":
            print(generate_password_with_length(args.length))
        elif arg == "template":
            print(generate_password_by_template(args.template))
        elif arg == "file":
            with open(args.file, "r") as file:
                templates = file.readlines()
                for line in templates:
                    print(generate_password_by_template(line))
                file.close()


if __name__ == "__main__":
    main()
