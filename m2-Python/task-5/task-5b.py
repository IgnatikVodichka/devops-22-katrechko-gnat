import argparse
import re
import datetime


parser = argparse.ArgumentParser()

parser.add_argument("file", metavar="<file>", action="store", type=str,  help="Getting file path <input-file>")
parser.add_argument("parameter", metavar="statistics", action="store", type=int,
                    help="What sub task to execute. Possible is from 1 to 10")
parser.add_argument("-top", "--top_items", metavar="top items", action="store", type=int, default=None,
                    help="How many sorted items to display. Default is all of them")
parser.add_argument("-dt", "--time_period", metavar="", action="store", type=int, default=1,
                    help="Time period in seconds. Default is per 1 second")
parser.add_argument("-s", "--slashes", type=int, default=2,
                    help="(Sub task 6) Up to how many slashes you want to parse after GET, POST etc. request. Default is 2")
parser.add_argument("-srtbitm", "--sort_by_item", action="store_true",
                    help="If passed to program sorts everything by item(ip, worker etc.,) if not - by amount of requests")
parser.add_argument("-rev", "--reverse", action="store_false", help="If passed to program reverses order of sort")

args = parser.parse_args()

source_file_path = args.file
parameter = args.parameter
n_top_items = args.top_items
how_many_slashes_in_request = args.slashes
user_time_input = args.time_period
sorted_by_item = args.sort_by_item
is_reverse = args.reverse


patterns = {
    "ip": r"(?<=\()((\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|unknown)(?=[\,\)])",
    "date/time": r"(\d\d/\w{3}/\d{4}:\d{2}:\d{2}:\d{2})",
    "request": r"((GET)?(POST)?(PUT)?(DELETE)?(HEAD)?(CONNECT)?(OPTIONS)?(TRACE)?(PATCH)? (([\w\-\*]*)/)" + "{" + f"{how_many_slashes_in_request}" + "})",
    "response": r"(\d{3}) (\d+|-) (\d+) (\d+)",
    "referer": r'(?<= ")(http(s)?://[a-z]?(www)?(\d)?((\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}):(\d+))?.[a-z]+.([a-z]+.[a-z]+)?(\w+)?)',
    "user_agent": r'(?<=" ")([^"])+(?=" ")',
    "worker": r'(?<=")\w*\:\/\/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{4}(?=")'
}


def main():
    """Executes a program number according to the task-5b written in PDF"""
    if parameter == 1:
        print_top_items(find_top_items("ip"), n_top_items, sorted_by_item)
    elif parameter == 2:
        sum_of_requests_per_user_time_period = sum(find_delta_time(find_top_items("date/time"), user_time_input))
        quantity_of_periods_of_time = len(find_delta_time(find_top_items("date/time"), user_time_input))
        frequency_of_requests_for_time_period = sum_of_requests_per_user_time_period / quantity_of_periods_of_time
        print(frequency_of_requests_for_time_period)
    elif parameter == 3:
        print_top_items(find_top_items("user_agent"), n_top_items, sorted_by_item)
    elif parameter == 4:
        sum_of_requests_per_user_time_period = sum(find_delta_time(find_top_items("date/time"), user_time_input))
        quantity_of_periods_of_time = len(find_delta_time(find_top_items("date/time"), user_time_input))
        frequency_of_requests_for_time_period = sum_of_requests_per_user_time_period / quantity_of_periods_of_time
        response_code_50x_frequency = find_code_of_request_quantity("response") / frequency_of_requests_for_time_period
        print(response_code_50x_frequency)
    elif parameter == 5:
        print_top_items(find_duration_of_connection("response"), n_top_items, sorted_by_item)
    elif parameter == 6:
        print_top_items(find_top_items("request"), n_top_items, sorted_by_item)
    elif parameter == 7:
        print_top_items(find_top_items("worker"), n_top_items, sorted_by_item)
    elif parameter == 8:
        print_top_items(find_top_items("referer"), n_top_items, sorted_by_item)
    elif parameter == 9:
        log_period = len(find_top_items("date/time"))
        for worker in sorted(find_top_items("worker"), key=lambda item: item[1], reverse=is_reverse):
            print(worker[0], (worker[1] / log_period) * user_time_input)
    elif parameter == 10:
        print(len(find_delta_time(find_top_items("date/time"), user_time_input)))


def line_reader(file_path: str) -> str:
    """Reads log and gives out line by line"""
    with open(file_path, "r") as log:
        for line in log:
            yield line


def print_top_items(statistics, top_items, sort_by_item=False) -> print:
    """Receives unsorted list and prints out sorted and N top items depending on the input of CLI"""
    if isinstance(statistics[0], tuple):
        for item in sorted(statistics, key=lambda i: i[0] if sort_by_item else i[1], reverse=is_reverse)[:top_items]:
            print(item[0], item[1])
    else:
        for item in sorted(statistics, reverse=is_reverse)[:top_items]:
            print(item)


def find_duration_of_connection(stat: str) -> list:
    """Makes a list containing connection length"""
    statistics_list = []
    for line in line_reader(source_file_path):
        try:
            duration_of_connection = re.search(patterns[stat], line).group().split()[-1]
        except AttributeError:
            continue
        statistics_list.append(int(duration_of_connection) / 1000)
    return statistics_list


def find_top_items(stat: str) -> list:
    """Forms a list containing tuples, where tuples first element is item and second element is amount"""
    statistics_dict = {}
    for line in line_reader(source_file_path):
        try:
            match = re.search(patterns[stat], line).group()
            if match not in statistics_dict:
                statistics_dict[match] = 1
            else:
                statistics_dict[match] += 1
        except AttributeError:
            continue
    statistics_list = [(x, statistics_dict[x]) for x in statistics_dict]
    return statistics_list


def find_code_of_request_quantity(stat: str) -> int:
    """Counts how many times 50X errors are in log"""
    codes_quantity = {}
    for line in line_reader(source_file_path):
        try:
            code = re.search(patterns[stat], line).group().split()[0]
        except AttributeError:
            continue
        if code in codes_quantity and code.startswith("50"):
            codes_quantity[code] += 1
        elif code not in codes_quantity and code.startswith("50"):
            codes_quantity[code] = 1
    return sum([codes_quantity[x] for x in codes_quantity])


def find_delta_time(sorted_list: list, time: int) -> list:
    """Receives a list containing unique date/time and the amount of its occurrences.
    Sorts it in the opposite order so that date/time will be stored from end to the beginning of the log.
    After that the list goes through the loop where date/time[this] subtracts date/time[next] and the result stored in a timer variable.
    Also amounts of unique date/time entries are summed up while timer is not equal to user specified time period
    As soon as timer is equal to user specified time period result is saved in the list and everything is reset.
    In the end we have a list with amounts of requests in dT(delta time) user specified period.
    """
    time_list = []
    time_diff = datetime.timedelta(seconds=0)
    time_period_requests = 0
    last_time_period_requests = 0
    sorted_list = sorted(sorted_list, key=lambda item: item[0], reverse=True)
    for index in range(len(sorted_list) - 1):
        user_time_period = datetime.timedelta(seconds=time)
        date_format = "%d/%b/%Y:%H:%M:%S"
        time_diff += datetime.datetime.strptime(sorted_list[index][0], date_format) - datetime.datetime.strptime(sorted_list[index + 1][0], date_format)
        time_period_requests += sorted_list[index][1]
        last_time_period_requests = time_period_requests
        if time_diff == user_time_period:
            time_diff = datetime.timedelta(seconds=0)
            time_list.append(time_period_requests)
            time_period_requests = 0
    time_list.append(last_time_period_requests + sorted_list[-1][1])
    return time_list


if __name__ == "__main__":
    main()
