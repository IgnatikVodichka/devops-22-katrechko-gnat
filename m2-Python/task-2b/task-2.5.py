

access_template = ['switchport mode access', 'switchport access vlan',
                   'spanning-tree portfast', 'spanning-tree bpduguard enable'
                   ]

trunk_template = ['switchport trunk encapsulation dot1q',
                  'switchport mode trunk', 'switchport trunk allowed vlan'
                  ]

access = {'0/12': '10',
          '0/14': '11',
          '0/16': '17',
          '0/17': '150'
          }

trunks = {'0/1': ['add', '10', '20'],
          '0/2': ['only', '11', '30'],
          '0/4': ['del', '17']
          }

for intf, vlan in trunks.items():
    print("interface FastEthernet" + intf)
    for command in trunk_template:
        if command.endswith("allowed vlan"):
            if vlan[0] == "add":
                print(f" {command} add ", end="")
                print(*vlan[1:], sep=", ")
            elif vlan[0] == "del":
                print(f" {command} remove ", end="")
                print(*vlan[1:], sep=", ")
            elif vlan[0] == "only":
                print(f" {command} only ", end="")
                print(*vlan[1:], sep=", ")
        else:
            print(' {}'.format(command))
