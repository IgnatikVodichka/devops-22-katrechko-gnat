import re


user_entered_ip = input("Please input IP address in format x.x.x.x: \n")

is_valid_ip = user_entered_ip.strip(".").split(".")
if len(is_valid_ip) != 4 or 255 > int(is_valid_ip[0]) < 0 or 255 > int(is_valid_ip[1]) < 0 or 255 > int(is_valid_ip[2]) < 0 or 255 > int(is_valid_ip[3]) < 0:
    print("Wrong")
elif user_entered_ip == "0.0.0.0":
    print("Unassigned")
elif user_entered_ip == "255.255.255.255":
    print("Local broadcast")
elif 1 <= int(is_valid_ip[0]) <= 223:
    print("Unicast")
elif 224 <= int(is_valid_ip[0]) <= 239:
    print("Multicast")
else:
    print("Unused")