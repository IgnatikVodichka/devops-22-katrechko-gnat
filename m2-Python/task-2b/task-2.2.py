
def check_ip(user_input):
    if user_entered_ip == "0.0.0.0":
        print("Unassigned")
    elif user_entered_ip == "255.255.255.255":
        print("Local broadcast")
    elif 1 <= int(user_entered_ip[:3]) <= 223:
        print("Unicast")
    elif 224 <= int(user_entered_ip[:3]) <= 239:
        print("Multicast")
    else:
        print("Unused")


user_entered_ip = input("Please input IP address in format x.x.x.x: \n")
if not user_entered_ip:
    print("Wrong")
else:
    check_ip(user_entered_ip)
