

def cisco_mac_modifier(mac: list):
    return [x.replace(":", ".") for x in mac]


macs = ['aabb:cc80:7000', 'aabb:dd80:7340', 'aabb:ee80:7000', 'aabb:ff80:7000']
mac_cisco = cisco_mac_modifier(macs)
print(mac_cisco)