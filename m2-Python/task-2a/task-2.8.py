

ip = "192.168.3.1"
formatted = ip.split(".")
print(f'''
{formatted[0]:<9}{formatted[1]:<9}{formatted[2]:<9}{formatted[3]:<9}
{int(formatted[0]):08b} {int(formatted[1]):08b} {int(formatted[2]):08b} {int(formatted[3]):08b}
''')
