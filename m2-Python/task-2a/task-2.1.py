

def interface_switcher(nat):
    interface1 = "Fast"
    interface2 = "Gigabit"
    return nat.replace(interface1, interface2) if interface1 in nat else nat


NAT = "ip nat inside source list ACL interface FastEthernet0/1 overload"

print(interface_switcher(NAT))
