

def string_formatter(string):
    string = string.strip().split()
    return f'''
    Protocol: {string[0]+"SPF":>15}
    Prefix: {string[1]:>25}
    AD/Metric: {string[2].strip("[]"):>16}
    Next-Hop: {string[4]:>21}
    Last update: {string[5]:>14}
    Outbound Interface: {string[6]:>16}
    '''


ospf_route = 'O 10.0.24.0/24 [110/41] via 10.0.13.3, 3d18h, FastEthernet0/0'
print(string_formatter(ospf_route))
