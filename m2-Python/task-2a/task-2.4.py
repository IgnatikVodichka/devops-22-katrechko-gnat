

def vlan_duplicates_remover(vlans):
    return sorted(list(set(vlans)))


vlans = [10, 20, 30, 1, 2, 100, 10, 30, 3, 4, 10]

print(vlan_duplicates_remover(vlans))
