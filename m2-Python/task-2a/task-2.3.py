

def vlan_extractor(config):
    return config.split()[-1].split(",")


config = "switchport trunk allowed vlan 1,3,10,20,30,100"

print(vlan_extractor(config))
