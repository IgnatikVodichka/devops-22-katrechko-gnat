import argparse
import csv
import re

parser = argparse.ArgumentParser()

parser.add_argument("-r", "--row", metavar="", action="store", type=str, help="Set row range to parse")
parser.add_argument("-c", "--column", metavar="", action="store", type=str, help="Set column range to parse")
parser.add_argument("-re", "--regex", metavar="", action="store", help="Regex raw string to filter through file")
parser.add_argument("-f", "--file", metavar="", action="store",
                    help="Getting file path <input-file> <output-file>(optional)")

args = parser.parse_args(["-f", "input.csv", "-r", "1-3,5", "-c", "1-3,5", "-re", "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"])

try:
    input_file, output_file = args.file.split()
except ValueError:
    input_file = args.file
    output_file = None
except AttributeError:
    parser.error("No input file given")

if not args.row:
    string_row_ranges = None
else:
    string_row_ranges = args.row.split(",")
if not args.column:
    string_column_ranges = None
else:
    string_column_ranges = args.column.split(",")


def main():
    if not string_row_ranges:
        row_numbers = None
    else:
        row_numbers = list_of_rows_columns(string_row_ranges)
    if not string_column_ranges:
        column_numbers = None
    else:
        column_numbers = list_of_rows_columns(string_column_ranges)

    regex_matches = []
    contents, headers = line_reader(input_file)
    print_rows_and_columns(row_numbers, column_numbers, contents, headers, regex_matches)
    for item in regex_matches:
        print(item)


def make_list_of_strings(string_of_ranges):
    numbers = []
    for i in string_of_ranges:
        numbers.append(i.split("-"))
    return numbers


def list_of_rows_columns(string):
    list_of_lists = make_list_of_strings(string)
    numbers = []
    for i in list_of_lists:
        if len(i) > 1:
            numbers.extend(
                range(int(i[0]), int(i[1]) + 1) if int(i[0]) < int(i[1]) else range(int(i[0]), int(i[1]) - 1, -1))
        else:
            numbers.append(int(i[0]))
    return numbers


def line_reader(path):
    contents = []
    with open(path, "r") as csv_file:
        reader = csv.reader(csv_file)
        headers = next(reader)
        for row in reader:
            contents.append(row)
    return contents, headers


def print_rows_and_columns(rows, columns, content, header, matches=None):
    if columns:
        for j in columns:
            print(f"{header[j - 1]}", end=", ")
        print("")
    else:
        if matches or not matches:
            for j in header:
                print(f"{j}", end=", ")
            print("")
    if rows and columns:
        for i in rows:
            for k in columns:
                if args.regex:
                    match = re.search(fr"{args.regex}", content[i - 1][k - 1])
                    if match:
                        matches.append(match.group())
                        continue
                else:
                    print(f"{content[i - 1][k - 1]}", end=", ")
            if not args.regex:
                print("")
    elif not rows and columns:
        for i in content:
            for k in columns:
                if args.regex:
                    match = re.search(fr"{args.regex}", i[k - 1])
                    if match:
                        matches.append(match.group())
                        continue
                else:
                    print(f"{i[k - 1]}", end=", ")
            if not args.regex:
                print("")
    elif not columns and rows:
        for i in rows:
            for col in content[i - 1]:
                if args.regex:
                    match = re.search(fr"{args.regex}", col)
                    if match:
                        matches.append(match.group())
                        continue
                else:
                    print(f"{col}", end=", ")
            if not args.regex:
                print("")
    else:
        for rows in content:
            for col in rows:
                if args.regex:
                    match = re.search(fr"{args.regex}", col)
                    if match:
                        matches.append(match.group())
                        continue
                else:
                    print(f"{col}", end=", ")
            if not args.regex:
                print("")
    return matches


if __name__ == "__main__":
    main()
