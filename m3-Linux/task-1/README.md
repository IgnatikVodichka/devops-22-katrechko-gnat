1. getent group {0..999} | cut -d: -f 1,3 --output-delimiter=" " > 1p.txt
2. getent group {0..999} | cut -d: -f 1,3 --output-delimiter=" " | xargs -n1 find / -group
3. find /home/* -type f -name *.sh
4. find / -type f -user root -name *.sh
5. grep -ri -n --include="*.sh" "linux" /
6. find / -type f -printf "%s\n" | sort | uniq -d | xargs -I{} -n1 find -type f -size {}c| xargs -n1 md5sum | sort | uniq -w32 -dD
7. find / -lname /home/vagrant/.bashrc
8. find / -samefile /home/vagrant/.bashrc
9. find / -type f -inum 100830460
10. sudo find $(mount -l | cut -d" " -f 3) -type f -inum 100830460
11. find / -samefile /home/vagrant/1p.txt 2>&- | xargs -n1 rm -i;
    find / -lname /home/vagrant/1p.txt 2>&- | xargs -n1 rm -i
12. chmod -R -rw-rw-rw- /home/vagrant/2p.txt
13. diff -qr /home/ /home/vagrant/ | grep "Only"
14. arp -a | cut -d" " -f 4
15. w | cut -d" " -f 1
16. netstat -tul
17. ln -sfT /home/vagrant/.bashrc /home/vagrant/bash.config
18. cat fileslist.txt | xargs -d" " -n1 echo | xargs -n2 sh -c 'mkdir $1 &> /dev/null; ln -frs $0 $1'
19. rsync -rLabvh vagrant/ vagrant.backup/
    cp -ard vagrant/ vagrant.backup.cp/
20. omitted
21. cp -a /home newhome/
22. find . -type l -exec bash -c 'realpath "{}" &> /dev/null && ln -sf "$(realpath "{}")" "{}"' \;
23. find . -type l -exec bash -c 'realpath "{}" &> /dev/null && ln -srf "$(realpath "{}")" "{}"' \;
24. find -L . -xtype l -delete
25. tar -xf (--lzma --xz --gzip --bzip2 --lzip) archive.tar file1.txt file2.txt -C /path/to/directory
26. tar -cf backup.tar /home/user
27. find /home/ -type d | xargs -I{} mkdir -p "./tmp/test/{}"
28. getent passwd | cut -d: -f 1 | sort
29. getent group | cut -d: -f 1,3 --output-delimiter=" " | sort -k 2n
30. getent group | cut -d: -f 1,3 --output-delimiter=" " | sort -k 2nr
31. grep 'nologin' /etc/passwd | cut -d: -f1
32. grep -E '/bin/bash' /etc/passwd | cut -d: -f1
    grep -v '/bin/bash' /etc/passwd | cut -d: -f1
33. curl -X GET www.google.com --silent | grep -Eo "(http|https)://[a-zA-Z0-9./?=_%:-]*"
    wget -qO- www.google.com | grep -Eo "(http|https)://[a-zA-Z0-9./?=_%:-]*"
34. ps -a --no-headers| cut -d" " -f 9 | xargs -n1 killall -o 5d
    find /proc -maxdepth 1 -regex '/proc/[0-9]*' -type d -mtime +5 -exec kill {} \;
35. 
36. hostname -I
37. grep -E -o "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" list-of-ips.txt
38. nmap -iL host-server.txt
    cat host-server.txt | xargs -n1 -P0 ping -c1|grep "bytes from"
    echo 10.0.2.{1..254}|xargs -n1 -P0 ping -c1|grep "bytes from"
    ip -f inet addr show | grep -Po 'inet \K[\d.]+' | xargs nmap -sn
39. omitted
40. openssl s_client -connect google.com:443 -showcerts < /dev/null | openssl x509 -text | grep -E 'DNS:\*(.+\.)+.+\..+$' -o

